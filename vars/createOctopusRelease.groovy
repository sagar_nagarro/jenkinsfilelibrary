// Function that writes a Nuspec file.
def call(Map parms) {
    writeFile file: "octopus.payload.json", text: """{
        "ProjectId": "${parms.projectId}",
        "ChannelId": "${parms.octopusChannelId}",
        "Version": "${parms.version}",
        "ReleaseNotes": "${parms.buildUrl}",
        "SelectedPackages": [
        {
            "StepName": "${parms.stepName}",
            "ActionName": "${parms.actionName}",
            "Version": "${parms.version}"
        }
        ]
    }"""

    // POST JSON file to Octopus using the Octopus REST API.
    sh "curl -k -X POST http://10.127.126.101/api/releases -H 'Content-Type: application/json' -H 'X-Octopus-ApiKey: API-9JFMDOG4BEGRLBAPYND5WDV9EEA' --data-binary @octopus.payload.json --trace-ascii octopus.log"
}