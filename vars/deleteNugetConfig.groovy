// Function that deletes an existing Nuget.config file.
import globals

def call() {
    String fileList = sh(
        script: 'ls -1 [Nn][Uu][Gg][Ee][Tt].[Cc][Oo][Nn][Ff][Ii][Gg]',
        returnStdout: true
    ).trim()

    if (fileList != '') {
        def files = fileList.split('\n')

        files.each { file ->
            sh "rm -f ${file}"
        }
    }
}
