import groovy.transform.TypeChecked
import globals

@TypeChecked
def call(Map parms) {
 
String artifactoryNugetUrl = "http://10.127.127.82:8082/artifactory/api/nuget/nuget"
String buildDotnetCommand
String projectFile = "CAF.Services.Configuration.Api.csproj"
String publishFolder = parms.get("publishFolder", "publish")
String publishPhysicalFolder 
String nuspecFileName = "CAF.Services.1.0.nuspec"
String nupkgFileName = "CAF.Services.1.0.nupkg"
String nuspecId = "CA"
String fullVersionNumber = "2.0"
String nuspecAuthor = "DHS"
String nuspecDescription = "CAF Server"
String artifactoryPublishUrl = "http://10.127.127.82:8082/artifactory/api/nuget/builds"



    
pipeline {
    agent any
    stages {
	stage('init') {
            steps {
                echo "init - Setup variables and print out values for debugging"
                script {
                       // Get environment values set by Jenkins.
                        branchName = "${env.BRANCH_NAME}"
                        buildNumber = "${env.BUILD_NUMBER}"
                        jobName = "${env.JOB_NAME}"
                        nodeName = "${env.NODE_NAME}"
                        workspaceFolder = "${env.WORKSPACE}"
                        buildUrl = "${env.RUN_DISPLAY_URL}"
                        buildUrlSlackMessage = "Click <${buildUrl}|here> to view job details"
                        buildUrlHtmlMessage = "<p>Click <a href=\'${buildUrl}\'>here</a> to view job details.</p>"
						
                        // Configure build
                        buildDotnetCommand += " /p:NoPackageAnalysis=true"
                        
                        publishPhysicalFolder = "${workspaceFolder}/${publishFolder}"
                        
						
						}
                        echo """
						artifactoryNugetUrl:$artifactoryNugetUrl
                        publishPhysicalFolder:$publishPhysicalFolder
						"""
						}
						}
	stage('install-dependencies-nuget') {
            
            steps {
                script {    
                    echo "Deleting any existing nuget.config files"
                    deleteNugetConfig
                    sh "rm -rf ${workspaceFolder}/pipeline-config"
                    // Create CI/CD nuget.config file in folder 'pipeline-config.'
                    sh "mkdir ${workspaceFolder}/pipeline-config"
                    nugetConfig = "${workspaceFolder}/pipeline-config/nuget.config"
                    echo "Writing Nuget config file"
                    //writeNugetConfig nugetConfig, , "${artifactoryNugetUrl}"
					writeNugetConfig nugetConfig , "${artifactoryNugetUrl}"
                }
            }
        }
        stage('build-dotnet') {
            
            steps {
                // Build the application.
            

                // Build the project.
               // echo "Building project ${projectFile} with config file ${nugetConfig}"
                sh "dotnet build ${projectFile}"
            }
        }
        stage('dist-nuget') {
            
            steps {
                // Package build outputs into the publish folder.
                sh "dotnet publish --configuration Release -o ${publishPhysicalFolder} --configfile ${nugetConfig} --source https://${artifactoryNugetUrl}"

                // Create the nuspec file and save it in the publish folder.
                script {
                    
                    writeNuspecFile publishPhysicalFolder, nuspecFileName, nuspecId, fullVersionNumber, nuspecAuthor, nuspecDescription

                    if (fileExists("${publishPhysicalFolder}/CAF.Services.2.0.nuspec")) {
                        echo 'Nuspec File written successfully.'
                    } else {
                        echo 'ERROR writing Nuspec file!'
                    }
                }
               
            }
        }
         stage('package-nuget') {
            
            
            steps {
                // Move to the publishPhysicalFolder folder and create the nupkg file in the workspace folder using zip.
                echo "Package up output in dist folder as a nuget"
                sh "cd ${publishFolder} && zip -r ${workspaceFolder}/${nupkgFileName} . && cd ${workspaceFolder}"
            }
        }
        stage('deploy-nuget') {
           
            steps {
                echo "Publish nuget build to Artifactory"
                sh "dotnet nuget push ${nupkgFileName} --api-key ${globals.artifactoryUserName}:${globals.artifactoryNugetPassword} --source ${artifactoryPublishUrl}"
                
            }
        }
		stage('Octopus-push-nuget') {
           
            steps {
                echo "Publish nuget build to Artifactory"
                sh "curl -X POST -k http://10.127.126.101/api/packages/raw -H 'X-Octopus-ApiKey: API-9JFMDOG4BEGRLBAPYND5WDV9EEA' -F data=@${nupkgFileName}"
                
            }
        }
        
	}
    post { 
        always { 
            echo 'I will always say Hello again!'
        }
    }
}
}