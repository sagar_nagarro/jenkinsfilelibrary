// Function that writes a Nuget.config file.
import globals

def call(String fileName, String artifactoryNugetUrl) {
    writeFile file: "${fileName}", text: """<?xml version="1.0" encoding="utf-8"?>
<configuration>
    <config>
        <add key="repositoryPath" value="%PACKAGEHOME%\\External" />
    </config>
    <packageSources>
        <add key="cafnugetserver" value="${artifactoryNugetUrl}" />
        <add key="nuget.org" value="https://api.nuget.org/v3/index.json" protocolVersion="3" />
    </packageSources>

    <!-- Used to store credentials -->
    <packageSourceCredentials>
        <cafnugetserver>
            <add key="Username" value="${globals.artifactoryUserName}" />
            <add key="ClearTextPassword" value="${globals.artifactoryClearTextPassword}" />
        </cafnugetserver>
    </packageSourceCredentials>

    <!-- Used to disable package sources  -->
    <disabledPackageSources />

</configuration>"""
}