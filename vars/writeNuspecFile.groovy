// Function that writes a Nuspec file.
def call(String folderName, String fileName, String nuspecId, String version, String author, String description) {
    writeFile file: "${folderName}/${fileName}", text: """<?xml version='1.0' encoding='utf-8'?>
<package xmlns='http://schemas.microsoft.com/packaging/2013/05/nuspec.xsd'>
  <metadata>
    <id>${nuspecId}</id>
    <version>${version}</version>
    <authors>${author}</authors>
    <description>${description}</description>
  </metadata>
</package>"""
}